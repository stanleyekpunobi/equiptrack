import { Component, OnInit } from '@angular/core';
import { ProductService } from '../shared/services/product.service';
import { ProductModel } from '../shared/models/product.model';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-maincontainer',
  templateUrl: './maincontainer.component.html',
  styleUrls: ['./maincontainer.component.css'],
  providers: [ProductService]
})
export class MaincontainerComponent implements OnInit {

  products: ProductModel[] = [];
  constructor(private productService: ProductService, private spinnerService: NgxSpinnerService) { }

  ngOnInit() {
    this.spinnerService.show();

    this.onGetAllProducts();

  }

  onGetAllProducts() {
    this.productService.GetAllProducts().subscribe(
      (data: ProductModel[]) => {
        for (const p of data) {
          this.products.push(p);
        }
        this.spinnerService.hide();

      },

      (error) => console.log(error)
    );
  }

}
