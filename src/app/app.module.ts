import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//material design
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatCardModule} from '@angular/material/card';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTableModule} from '@angular/material/table';

//ng bootstrap
import { NgbDatepickerModule, NgbDropdownModule, NgbModalModule, NgbCollapseModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgxCoolDialogsModule } from 'ngx-cool-dialogs';

//components
import { AppComponent } from './app.component';
import { MaincontainerComponent } from './maincontainer/maincontainer.component';
import { AddproductComponent } from './product/addproduct/addproduct.component';
import { ViewproductComponent } from './product/viewproduct/viewproduct.component';
import { ExpensesComponent } from './expenses/expenses.component';
import { AddexpenseComponent } from './expenses/addexpense/addexpense.component';
import { EditexpenseComponent } from './expenses/editexpense/editexpense.component';
import { SalesComponent } from './sales/sales.component';
import { AddsalesComponent } from './sales/addsales/addsales.component';
import { EditsalesComponent } from './sales/editsales/editsales.component';
import { ProductService } from './shared/services/product.service';
import { ExpenseService } from './shared/services/expense.service';
import { DropdownDirective } from './shared/directives/dropdown.directive';
import { NotFoundComponent } from './not-found/not-found.component';
import { EditproductComponent } from './product/editproduct/editproduct.component';
import { ExpensecategoryComponent } from './expenses/expensecategory/expensecategory.component';
import { EditexpensecategoryComponent } from './expenses/expensecategory/editexpensecategory/editexpensecategory.component';
import { AddpaymentsComponent } from './sales/addpayments/addpayments.component';
import { ViewpaymentsComponent } from './sales/viewpayments/viewpayments.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

const appRoutes: Routes = [
  { path: '', component: MaincontainerComponent },
  { path: 'add-product', component: AddproductComponent },
  { path: 'view-product', component: ViewproductComponent },
  { path: 'edit-product/:id/:isedit', component: AddproductComponent },
  { path: 'edit-expense', component: EditexpenseComponent },
  { path: 'add-expense/:id', component: AddexpenseComponent },
  { path: 'expense-category', component: ExpensecategoryComponent },
  { path: 'view-expenses/:id', component: ExpensesComponent },
  { path: 'view-sales/:id', component: SalesComponent },
  { path: 'add-sales', component: AddsalesComponent },
  { path: 'add-payment/:id', component: AddpaymentsComponent },
  // { path: 'view-paments', component: ViewpaymentsComponent },
  { path: '404-page', component: NotFoundComponent },
  { path: '**', redirectTo: '/404-page' }
];

@NgModule({
  declarations: [
    AppComponent,
    MaincontainerComponent,
    AddproductComponent,
    ViewproductComponent,
    ExpensesComponent,
    AddexpenseComponent,
    EditexpenseComponent,
    SalesComponent,
    AddsalesComponent,
    EditsalesComponent,
    DropdownDirective,
    NotFoundComponent,
    EditproductComponent,
    ExpensecategoryComponent,
    EditexpensecategoryComponent,
    AddpaymentsComponent,
    ViewpaymentsComponent,
    MainNavComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes, { useHash: true }),
    NgxCoolDialogsModule.forRoot(),
    MatMenuModule,
    MatIconModule,
    MatFormFieldModule,
    MatSidenavModule,
    MatGridListModule,
    MatCardModule,
    MatSelectModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatExpansionModule,
    MatDialogModule,
    MatTableModule,
    NgbDatepickerModule,
    NgbDropdownModule,
    NgbModalModule,
    NgbCollapseModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatListModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],

  providers: [ProductService, ExpenseService, NgbActiveModal, MatDatepickerModule],
  bootstrap: [AppComponent],
  entryComponents: [EditexpenseComponent, EditexpensecategoryComponent, AddpaymentsComponent, EditsalesComponent]
})
export class AppModule { }
