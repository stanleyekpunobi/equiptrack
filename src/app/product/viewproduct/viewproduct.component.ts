import { Component, OnInit } from "@angular/core";
import { ProductService } from "src/app/shared/services/product.service";
import { ProductModel } from "src/app/shared/models/product.model";
import { NgxSpinnerService } from "ngx-spinner";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import { OperationModel } from "src/app/shared/models/operationmodel.model";
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import { AddpaymentsComponent } from "src/app/sales/addpayments/addpayments.component";
import { AddsalesComponent } from "src/app/sales/addsales/addsales.component";
import { AddexpenseComponent } from "src/app/expenses/addexpense/addexpense.component";

@Component({
  selector: "app-viewproduct",
  templateUrl: "./viewproduct.component.html",
  styleUrls: ["./viewproduct.component.css"],
  providers: [ProductService]
})
export class ViewproductComponent implements OnInit {
  panelOpenState = false;
  products: ProductModel[] = [];
  breakpoint: number;

  constructor(
    private productService: ProductService,
    private spinnerService: NgxSpinnerService,
    private modalService: NgbModal, private dialog: NgxCoolDialogsService,
    private matDialog: MatDialog
  ) { }

  ngOnInit() {
    this.spinnerService.show();

    this.onGetAllProducts();
    console.log(this.products);
    this.breakpoint = (window.innerWidth <= 415) ? 1 : 4;

  }

  onResize(event) {
    this.breakpoint = (event.target.innerWidth <= 415) ? 1 : 4;
  }

  openDialog(product:ProductModel) : void {
    const dialogRef = this.matDialog.open(AddpaymentsComponent, {
      width:'400px',
      data: product
    });

  }

  openSaleDialog(product: ProductModel) : void {
    const dialogRef = this.matDialog.open(AddsalesComponent, {
      width:'400px',
      data: product
    });
  }

  openExpenseDialog(product: ProductModel) : void {
    const dialogRef = this.matDialog.open(AddexpenseComponent, {
      width:'400px',
      data: product
    });
  }


  onDeleteProduct(productId) {

    // console.log(productid);
    const operationModel = new OperationModel(productId);

    this.dialog.confirm('This will delete the selected product')
      .subscribe(res => {
        if (res) {
          this.productService.DeleteProduct(operationModel).subscribe(
            (response) => {
              this.products.length = 0;
              this.onGetAllProducts();
              this.dialog.alert('Product deleted', {
                theme: 'default',
                title: 'Auction dash'
              });
            },
            (error) => this.dialog.alert('Unable to update product')
          );
        } else {
          this.dialog.alert('Delete operation canceled');
        }
      });

  }

  onGetAllProducts() {
    this.productService.GetAllProducts().subscribe(
      (data: ProductModel[]) => {
        console.log(data);

        for (let p of data) {
          var newproduct = this.AnalyzeData(p);
          this.products.push(newproduct);
        }
        this.spinnerService.hide();
        // console.log(this.products);
      },
      error => console.log(error)
    );
  }

   AnalyzeData (p: ProductModel): ProductModel {
    p.TotalExpenses = 0;
    p.TotalPayments = 0;
    p.TotalSale = 0;
    p.TotalPayments = 0;
    p.IsProfit = false;

    if (p.Expenses != null) {

      p.Expenses.forEach(expense => {
        p.TotalExpenses = expense.Amount + p.TotalExpenses;
      });

    }

    if (p.Sale != null) {

      p.TotalSale = p.Sale.Amount;

      if (p.Sale.Payments != null) {

        p.Sale.Payments.forEach(payments => {
          p.TotalPayments = payments.AmountPaid + p.TotalPayments;
        });
      }


      if (p.TotalPayments > p.TotalExpenses) {
        p.IsProfit = true;

      }

    }


    return p;

  }
}
