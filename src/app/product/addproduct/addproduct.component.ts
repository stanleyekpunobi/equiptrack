import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ProductService } from 'src/app/shared/services/product.service';
import { ProductModel } from 'src/app/shared/models/product.model';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { Router, ActivatedRoute } from '@angular/router';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.css']
})
export class AddproductComponent implements OnInit {
  @ViewChild('pDate') datePicker : ElementRef;
  Imagesrc = 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/768px-No_image_available.svg.png';
  productId: number;
  productModel : ProductModel = new ProductModel(null, null, null, null, null, null, null,null, '', null, null, null, null, null, null);
  isEditMode:boolean = false;
  constructor(private productService: ProductService, private dialog: NgxCoolDialogsService, private router: Router, private activateRoute: ActivatedRoute) { 
  }

  ngOnInit() {
    this.productId = this.activateRoute.snapshot.params["id"];
    let editParam = this.activateRoute.snapshot.params["isedit"];
    if(editParam == 1){
      this.isEditMode = true;
      this.onGetProduct(this.productId);
      console.log(this.productModel.Image);
      this.Imagesrc = this.productModel.Image;
    }
    else if(editParam == undefined){
      this.isEditMode = false;
    }
  }

  inputSelected(e) {
    // const file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    // const pattern = /image-*/;
    // const reader = new FileReader();
    // if (!file.type.match(pattern)) {
    //   this.dialog.alert('Invalid image format selected');
    //   return;
    // }
    // this.loadImage.bind(this);
    // reader.readAsDataURL(file);
    // this.Imagesrc = e.value;
    // console.log(e);
    
    if(e.target.value == ''){
      this.Imagesrc = 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/768px-No_image_available.svg.png'
    }
    else{
      this.Imagesrc = e.target.value;
    }
  }

  // loadImage(e) {
  //   const reader = e.target;
  //   this.Imagesrc = reader.result;
  // }

  onGetProduct(id){
    this.productService.GetProduct(id).subscribe(
      (data: ProductModel) => {
        console.log(data);
        this.productModel = data;
      }
    )
  }

  AddProduct(form: NgForm) {

    console.log(this.datePicker.nativeElement.value);

    const purchaseDate = formatDate(this.datePicker.nativeElement.value, 'yyyy-MM-dd', 'en-US');
    
    console.log(purchaseDate);

    const newproductModel = new ProductModel(null, form.value.EquipmentType, form.value.Model, form.value.Year,
      form.value.ProductName, form.value.VinNumber, form.value.PurchaseLocation, purchaseDate, this.Imagesrc, 
      null, null, null, null, null, null);

     this.productService.AddProduct(newproductModel).subscribe(
      (response) => this.dialog.alert('New product successfuly created!', {
        theme: 'default',
        title: 'Auction dash'
      }).subscribe(
        (res) => {
          if (res) {
            this.router.navigate(['/view-product']);
          } else {
            return null;
          }
        }
      ),
      (error) => this.dialog.alert('Unable to create product')
    );

  }

}
