import { Component, OnInit, ViewChild, ElementRef, Inject } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ExpenseService } from 'src/app/shared/services/expense.service';
import { ExpenseModel } from 'src/app/shared/models/expense.model';
import { NgForm, FormGroup, FormControl } from '@angular/forms';
import { ExpenseCategoryModel } from 'src/app/shared/models/expensecategory.model';
import { ProductService } from 'src/app/shared/services/product.service';
import { ProductModel } from 'src/app/shared/models/product.model';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { Router, ActivatedRoute } from '@angular/router';
import { formatDate } from '@angular/common';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ExpensesComponent } from '../expenses.component';
@Component({
  selector: 'app-addexpense',
  templateUrl: './addexpense.component.html',
  styleUrls: ['./addexpense.component.css'],
  providers: [ExpenseService]
})
export class AddexpenseComponent implements OnInit {

  productModel: ProductModel;

  expenseCategoryModel: ExpenseCategoryModel[] = [];;

  imageSrc = '';

  productId: number;

  expenseCategory;

  isEditMode: boolean = false;

  editForm: FormGroup;

  @ViewChild('eDate') datePicker : ElementRef;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,public matDialogRef: MatDialogRef<AddexpenseComponent>, private expenseService: ExpenseService, private productService: ProductService,
    private modalService: NgbModal, private dialog: NgxCoolDialogsService,
    private router: Router, private activeRoute: ActivatedRoute, public matdialog: MatDialog, ) {

  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' });
  }

  openDialog(content) :void {
    const dialogRef = this.matdialog.open(content, {
      width: '250px',
      data : {Category: this.expenseCategory}
    })

  }

  closeDialog(content): void {
    const dialogRef = this.matdialog.closeAll();
  }

  ngOnInit() {
    console.log(this.data);

    this.productId = this.activeRoute.snapshot.params['id'];
    this.onGetProduct(this.productId);
    this.onGetAllExpenseCategory();
    if(this.data != null) {
      if(this.data.isedit == '1'){

        console.log(this.data);
        this.isEditMode = true;
        this.editForm = new FormGroup({
          'Category': new FormControl(this.data.expensecategory.Id.toString()),
          'Amount': new FormControl(this.data.productexpense.Amount),
          'Currency': new FormControl(this.data.productexpense.Currency),
          'Description': new FormControl(this.data.productexpense.Description),
          'ExpenseDate': new FormControl(this.data.productexpense.ExpenseDate),
          'ExpenseId': new FormControl(this.data.productexpense.Id)
        })

      }


    }

  }

  onProductSelected(event: Event) {
    console.log((<HTMLSelectElement>event.target).value.split('>', 2)[1]);
    this.imageSrc = (<HTMLSelectElement>event.target).value.split('>', 2)[1];
  }

  onGetProduct(id){
    this.productService.GetProduct(id).subscribe(
      (data: ProductModel) => {
        //console.log(data);
        this.productModel = data;
      }
    )
  }

  onGetAllExpenseCategory() {
    this.expenseService.GetAllExpensesCategory().subscribe(
      (data: ExpenseCategoryModel[]) => {
        for (const c of data) {
          this.expenseCategoryModel.push(c);
        }
      }

    );
  }

  OnAddExpense(form: NgForm) {
    console.log(this.data);

   if(this.data != null){
     this.productId = this.data.Id;
   }
   console.log(this.productId);
   // const expenseDate = new Date(form.value.ExpenseDate.year, form.value.ExpenseDate.month - 1, form.value.ExpenseDate.day);
    const expenseDate = formatDate(this.datePicker.nativeElement.value, 'yyyy-MM-dd', 'en-US');

    const expenseModel = new ExpenseModel(this.productId, form.value.Amount, form.value.Currency,
      form.value.Description, expenseDate, form.value.EquipmentType, null, null);
    this.expenseService.AddExpense(expenseModel).subscribe(
      (response) => this.dialog.alert('Expense added to product', {
        theme: 'default',
        title: 'Auction dash'
      }).subscribe(
        (res) => {
          if (res) {
            this.router.navigate(['/view-expenses', expenseModel.ProductId]);
          }
        }
      ),
      (error) => this.dialog.alert('Unable to create product')
    );

  }

  OnAddExpenseCategory(form: NgForm) {

    const expenseCategoryModel = new ExpenseCategoryModel(null, form.value.CategoryName, null);

    this.expenseService.AddExpenseCategory(expenseCategoryModel).subscribe(
      (response) => this.dialog.alert('Expense category created', {
        theme: 'default',
        title: 'Auction dash'
      }).subscribe(
        (res) => {
          if (res) {
            this.expenseCategoryModel.splice(0, this.expenseCategoryModel.length);
            this.onGetAllExpenseCategory();
          }
        }
      ),
      (error) => this.dialog.alert('Unable to create product')
    );

  }

  OnEditExpense(form: NgForm) {
    console.log(form.value);
    const expenseDate = formatDate(this.datePicker.nativeElement.value, 'yyyy-MM-dd', 'en-US');

    const expenseModel = new ExpenseModel(this.productId, form.value.Amount, form.value.Currency,
      form.value.Description, expenseDate, form.value.Category, null, form.value.ExpenseId);
    this.expenseService.EditExpense(expenseModel).subscribe(
      (response) => this.dialog.alert('Change Successful', {
        theme: 'default',
        title: 'Auction dash'
      }).subscribe(
        (res) => {
          if (res) {
            this.matDialogRef.close();
          }
        }
      ),
      (error) => this.dialog.alert('Unable to create product')
    );
  }

}
