import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ExpenseCategoryModel } from 'src/app/shared/models/expensecategory.model';
import { NgbActiveModal, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { ExpenseService } from 'src/app/shared/services/expense.service';
import { NgForm } from '@angular/forms';
import { formatDate } from '@angular/common';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';

@Component({
  selector: 'app-editexpensecategory',
  templateUrl: './editexpensecategory.component.html',
  styleUrls: ['./editexpensecategory.component.css']
})
export class EditexpensecategoryComponent implements OnInit, AfterViewInit {

  expenseCategoryModel: ExpenseCategoryModel;


  dateCreated = {
    year: 0,
    month: 10,
    day: 19
  };

  constructor(public activeModal: NgbActiveModal, private expenseService: ExpenseService, private dialog: NgxCoolDialogsService) { }

  ngOnInit() {
  }

  ngAfterViewInit() {

    const dateString = formatDate(this.expenseCategoryModel.DateCreated.toString(), 'yyyy-MM-dd', 'en-US');
    const splitDate = dateString.split('-');
    this.dateCreated.year = parseInt(splitDate[0], 10);
    this.dateCreated.month = parseInt(splitDate[1], 10);
    this.dateCreated.day = parseInt(splitDate[2], 10);
    console.log(this.expenseCategoryModel);

  }

  onEditExpenseCategory(formData: NgForm) {

    this.expenseCategoryModel.CategoryName = formData.value.CategoryName;

    this.expenseService.EditExpenseCategory(this.expenseCategoryModel).subscribe(

      (response) => this.dialog.alert('Expense Category successfuly changed', {
        theme: 'default',
        title: 'Auction dash'
      }).subscribe(
        (res) => {
          if (res) {
            this.activeModal.dismiss('Update complete');
          }
        }),
      (error) => this.dialog.alert('Unable to update expense category')
    );

  }

}
