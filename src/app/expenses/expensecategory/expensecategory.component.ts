import { Component, OnInit } from '@angular/core';
import { ExpenseCategoryModel } from 'src/app/shared/models/expensecategory.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EditproductComponent } from 'src/app/product/editproduct/editproduct.component';
import { ExpenseService } from 'src/app/shared/services/expense.service';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { EditexpensecategoryComponent } from './editexpensecategory/editexpensecategory.component';
import { OperationModel } from 'src/app/shared/models/operationmodel.model';

@Component({
  selector: 'app-expensecategory',
  templateUrl: './expensecategory.component.html',
  styleUrls: ['./expensecategory.component.css']
})
export class ExpensecategoryComponent implements OnInit {

  expenseCategoryList: ExpenseCategoryModel[] = [];

  constructor(private modalService: NgbModal, private expenseService: ExpenseService, private dialog: NgxCoolDialogsService) { }

  ngOnInit() {
    this.onGetAllCategory();
  }

  open(expenseCategoryModel: ExpenseCategoryModel) {
    const modalRef = this.modalService.open(EditexpensecategoryComponent);
    modalRef.componentInstance.expenseCategoryModel = expenseCategoryModel;

  }

  onDeleteCategory(categoryId) {
    const operationModel = new OperationModel(categoryId);

    this.dialog.confirm('This will delete the selected expense category')
      .subscribe(res => {
        if (res) {
          this.expenseService.DeleteExpenseCategory(operationModel).subscribe(
            (response) => {
              this.expenseCategoryList.length = 0;
              this.onGetAllCategory();
              this.dialog.alert('Expense category deleted', {
                theme: 'default',
                title: 'Auction dash'
              });
            },
            (error) => this.dialog.alert('Unable to update expense category')
          );
        } else {
          this.dialog.alert('Delete operation canceled');
        }
      });

    // console.log(expenseId);
  }

  onGetAllCategory() {
    this.expenseService.GetAllExpensesCategory().subscribe(
      (res: ExpenseCategoryModel[]) => {
        for (const e of res) {
          this.expenseCategoryList.push(e);
        }
      },
      (error) => this.dialog.alert('Unable to fetch catogry list')
    );
  }

}
