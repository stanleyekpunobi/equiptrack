import { Component, OnInit, Input, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { ExpenseModel } from 'src/app/shared/models/expense.model';
import { ExpenseService } from 'src/app/shared/services/expense.service';
import { ExpenseCategoryModel } from 'src/app/shared/models/expensecategory.model';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { formatDate, DatePipe } from '@angular/common';
import { NgForm } from '@angular/forms';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';

@Component({
  selector: 'app-editexpense',
  templateUrl: './editexpense.component.html',
  styleUrls: ['./editexpense.component.css'],
  providers: [DatePipe]
})
export class EditexpenseComponent implements OnInit, AfterViewInit {

  @Input() expenseModel: ExpenseModel = new ExpenseModel(null, null, null, null, null, null, null, null);

  @ViewChild('expenseCatId') expenseCategoryId: ElementRef;

  @ViewChild('currency') currency: ElementRef;

  expenseDate = {
    year: 0,
    month: 10,
    day: 19
  };

  expenseCategoryModel: ExpenseCategoryModel[] = [];

  currencyUsed = ['NGN', 'USD'];


  constructor(private expenseService: ExpenseService, public activeModal: NgbActiveModal, private dialog: NgxCoolDialogsService) { }

  ngOnInit() {

  }

  ngAfterViewInit() {
    this.onGetAllExpenseCategory();
    // console.log(this.expenseModel);
    const dateString = formatDate(this.expenseModel.ExpenseDate.toString(), 'yyyy-MM-dd', 'en-US');
    const splitDate = dateString.split('-');
    this.expenseDate.year = parseInt(splitDate[0], 10);
    this.expenseDate.month = parseInt(splitDate[1], 10);
    this.expenseDate.day = parseInt(splitDate[2], 10);

  }

  onGetExpenseCategoryValue(val1: any, val2: any): boolean {

    return val1 && val2 ? val1 === val2 : val1 === val2;
  }

  onGetAllExpenseCategory() {
    this.expenseService.GetAllExpensesCategory().subscribe(
      (data: ExpenseCategoryModel[]) => {

        for (const c of data) {
          this.expenseCategoryModel.push(c);
        }
      }
    );

  }

  onEditExpense(form: NgForm) {

    this.expenseModel.Amount = form.value.Amount;
    this.expenseModel.ExpenseCategoryId = this.expenseCategoryId.nativeElement.value;
    this.expenseModel.Currency = this.currency.nativeElement.value;
    this.expenseModel.Description = form.value.Description;
    //this.expenseModel.ExpenseDate = new Date(this.expenseDate.year, this.expenseDate.month, this.expenseDate.day);


    this.expenseService.EditExpense(this.expenseModel).subscribe(
      (response) => this.dialog.alert('Expense successfuly changed', {
        theme: 'default',
        title: 'Auction dash'
      }).subscribe(
        (res) => {
          if (res) {
            this.activeModal.dismiss('Update complete');
          }
        }),
      (error) => this.dialog.alert('Unable to update expense')
    );
  }

}
