import { Component, OnInit, ViewChild } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';

import { ExpenseService } from '../shared/services/expense.service';
import { ViewProductExpenseModel } from '../shared/models/viewproductexpense.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EditexpenseComponent } from './editexpense/editexpense.component';
import { ExpenseModel } from '../shared/models/expense.model';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { OperationModel } from '../shared/models/operationmodel.model';
import { ProductModel } from '../shared/models/product.model';
import {MatDialog} from "@angular/material/dialog";
import { AddexpenseComponent } from "src/app/expenses/addexpense/addexpense.component";
import { ExpenseCategoryModel } from '../shared/models/expensecategory.model';


@Component({
  selector: 'app-expenses',
  templateUrl: './expenses.component.html',
  styleUrls: ['./expenses.component.css'],
  providers: [ExpenseService]
})


export class ExpensesComponent implements OnInit {

  dataSource : MatTableDataSource<ExpenseModel>;
  panelOpenState: boolean = false;
  expenseCategoryModel: ExpenseCategoryModel[] = [];;


  constructor(private expenseService: ExpenseService, private spinnerService: NgxSpinnerService, private activeRoute: ActivatedRoute,
    private modalService: NgbModal, private dialog: NgxCoolDialogsService,  private matDialog: MatDialog) {

      this.dataSource = new MatTableDataSource<ExpenseModel>();

     }


 displayedColumns: string[] = ['category', 'amount', 'description', 'date', 'action'];


  productExpense: ViewProductExpenseModel =
    {
      EquipmentType: '',
      Expenses: [],
      Year: 0,
      Model: '',
      Image: '',
      ProductId: 0,
      ProductName: '',
      PurchaseDate: new Date(),
      PurchaseLocation: '',
      VinNumber: ''
    };



  // tslint:disable-next-line:no-inferrable-types
  totalExpenses: number = 0;

  ngOnInit() {
    const productId = this.activeRoute.snapshot.params['id'];
    this.spinnerService.show();
    this.onGetExpenseForProduct(productId);
    this.onGetAllExpenseCategory();
  }

  open(expense: ExpenseModel) {
    const editModalRef = this.modalService.open(EditexpenseComponent);
    //console.log(expense);
    editModalRef.componentInstance.expenseModel = expense;
    // console.log(editModalRef.componentInstance.amount);
  }

  openExpenseDialog(expense: any, isEdit: number) : void {

    let category;
    for(const c of this.expenseCategoryModel){

      if(c.CategoryName == expense.Category){
        category = c;
        break;
       }
    }
    const productId = this.activeRoute.snapshot.params['id']
    const dialogRef = this.matDialog.open(AddexpenseComponent, {
      width:'500px',
      data: {productexpense:expense, isedit:isEdit, productid: productId, expensecategory: category}
    });

    dialogRef.afterClosed().subscribe((result) => {
      let productid = this.activeRoute.snapshot.params['id'];
      this.onGetExpenseForProduct(productid);
    })

  }

  openAddExpenseDialog(){
    const productId = this.activeRoute.snapshot.params['id'];
    const dialogRef = this.matDialog.open(AddexpenseComponent, {
      width:'500px',
      data: {productid: productId}
    });
    dialogRef.afterClosed().subscribe((result) => {
      let productid = this.activeRoute.snapshot.params['id'];
      this.onGetExpenseForProduct(productid);
    })
  }

  onGetAllExpenseCategory() {
    this.expenseService.GetAllExpensesCategory().subscribe(
      (data: ExpenseCategoryModel[]) => {
        for (const c of data) {
          this.expenseCategoryModel.push(c);
        }
      }

    );
  }

  onDeleteExpense(expenseId) {

    // console.log(productid);
    const operationModel = new OperationModel(expenseId);

    this.dialog.confirm('This will delete the selected expense for this product')
      .subscribe(res => {
        if (res) {
          this.expenseService.DeleteExpense(operationModel).subscribe(
            (response) => {
              this.productExpense.Expenses.length = 0;
              const productId = this.activeRoute.snapshot.params['id'];
              this.onGetExpenseForProduct(productId);
              this.dialog.alert('Expense deleted', {
                theme: 'default',
                title: 'Auction dash'
              });
            },
            (error) => this.dialog.alert('Unable to update expense')
          );
        } else {
          this.dialog.alert('Delete operation canceled');
        }
      });

    //console.log(expenseId);

  }

  onGetExpenseForProduct(productid: number) {
    //const expenses: ExpenseModel[];

    this.expenseService.GetExpensebyProduct(productid).subscribe(

      (data: ViewProductExpenseModel) => {
        for (const e of data.Expenses) {
          this.productExpense.Expenses.push(e);

          this.totalExpenses += e.Amount;
        }
        this.dataSource.data = data.Expenses;

        this.productExpense.EquipmentType = data.EquipmentType;
        this.productExpense.Image = data.Image;
        this.productExpense.Model = data.Model;
        this.productExpense.ProductId = data.ProductId;
        this.productExpense.ProductName = data.ProductName;
        this.productExpense.PurchaseDate = data.PurchaseDate;
        this.productExpense.PurchaseLocation = data.PurchaseLocation;
        this.productExpense.VinNumber = data.VinNumber;
        this.productExpense.Year = data.Year;
        this.spinnerService.hide();
        //console.log(this.totalExpenses);



      },
      (error) => console.log(error)
    );
  }

}
