import { SaleModel } from '../models/sale.model';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { map } from 'rxjs/operators';
import { PaymentModel } from '../models/payment.model';
import { OperationModel } from '../models/operationmodel.model';
@Injectable()
export class SaleService {

    constructor(private httpClient: Http) { }

    AddSale(model: SaleModel) {
        return this.httpClient.post('http://honors1000-001-site4.dtempurl.com/api/Sales/AddSale', model);
    }

    UpdateSale(model: SaleModel) {
        return this.httpClient.post('http://honors1000-001-site4.dtempurl.com/api/Sales/UpdateSale', model);
    }

    DeleteSale(model: OperationModel) {
        return this.httpClient.post('http://honors1000-001-site4.dtempurl.com/api/Sales/DeleteSale', model);

    }

    GetAllSales() {
    }

    GetSale(saleid: number) {
        return this.httpClient.get('http://honors1000-001-site4.dtempurl.com/api/Sales/GetSale?id=' + saleid).pipe(
            map(
                (response: Response) => {
                    const data = response.json();
                    return data;
                }
            )
        );
    }

    AddPayment(model: PaymentModel) {
        return this.httpClient.post('https://localhost:44308/api/Sales/AddPayment', model);
    }

    UpdatePayment(model: PaymentModel) {
        return this.httpClient.post('http://honors1000-001-site4.dtempurl.com/api/Sales/UpdatePayment', model);
    }

    DeletePayment(model: OperationModel) {
        return this.httpClient.post('http://honors1000-001-site4.dtempurl.com/api/Sales/DeletePayment', model);
    }
}
