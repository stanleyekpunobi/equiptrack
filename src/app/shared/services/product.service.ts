import { ProductModel } from "../models/product.model";
import { Http, Response } from "@angular/http";
import { Injectable } from "@angular/core";
import { map } from "rxjs/operators";
import { OperationModel } from "../models/operationmodel.model";
@Injectable()
export class ProductService {
  constructor(private httpClient: Http) {}

  AddProduct(model: ProductModel) {
    return this.httpClient.post(
      "http://honors1000-001-site4.dtempurl.com/api/Product/AddProduct",
      model
    );
  }

  EditProduct(model: ProductModel) {}

  DeleteProduct(model: OperationModel) {
    return this.httpClient.post('http://honors1000-001-site4.dtempurl.com/api/Product/DeleteProduct', model);

  }

  GetAllProducts() {
    return this.httpClient
      .get("http://honors1000-001-site4.dtempurl.com/api/Product/GetAllProducts")
      .pipe(
        map((response: Response) => {
          const data = response.json();
          return data.product;
        })
      );
  }

  GetProduct(productid: number) {
    return this.httpClient
    .get("http://honors1000-001-site4.dtempurl.com/api/Product/GetProduct?id=" + productid)
    .pipe(
      map((response: Response) => {
        const data = response.json();
        return data.product;
      })
    );


  }
}
