import { ExpenseModel } from '../models/expense.model';
import { ExpenseCategoryModel } from '../models/expensecategory.model';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { map } from 'rxjs/operators';
import { OperationModel } from '../models/operationmodel.model';
@Injectable()
export class ExpenseService {

    constructor(private httpClient: Http) { }

    AddExpense(model: ExpenseModel) {
        return this.httpClient.post('http://honors1000-001-site4.dtempurl.com/api/Expense/AddExpense', model);
    }

    EditExpense(model: ExpenseModel) {
        return this.httpClient.post('http://honors1000-001-site4.dtempurl.com/api/Expense/UpdateExpense', model);

    }

    DeleteExpense(model: OperationModel) {
        return this.httpClient.post('http://honors1000-001-site4.dtempurl.com/api/Expense/DeletExpense', model);

    }

    GetAllExpenses() {

    }

    GetExpensebyProduct(productid: number) {
        return this.httpClient.get('http://honors1000-001-site4.dtempurl.com/api/Expense/GetExpenseByProduct?id=' + productid).pipe(
            map(
                (response: Response) => {
                    const data = response.json();
                    return data.productExpense;
                }
            )
        );
    }

    AddExpenseCategory(model: ExpenseCategoryModel) {
        return this.httpClient.post('http://honors1000-001-site4.dtempurl.com/api/Expense/AddExpenseCategory', model);
    }

    EditExpenseCategory(model: ExpenseCategoryModel) {
        return this.httpClient.post('http://honors1000-001-site4.dtempurl.com/api/Expense/UpdateExpenseCategory', model);

    }

    DeleteExpenseCategory(model: OperationModel) {
        return this.httpClient.post('http://honors1000-001-site4.dtempurl.com/api/Expense/DeletExpenseCategory', model);
    }

    GetAllExpensesCategory() {
        return this.httpClient.get('http://honors1000-001-site4.dtempurl.com/api/Expense/GetAllExpenseCategory').pipe(
            map(
                (response: Response) => {
                    const data = response.json();
                    return data.category;
                }
            )
        );
    }

    GetExpenseCategory(categoryid: number) {

    }
}
