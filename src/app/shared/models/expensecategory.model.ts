export class ExpenseCategoryModel {

    constructor(public Id: number, public CategoryName: string, public DateCreated: Date) { }
}
