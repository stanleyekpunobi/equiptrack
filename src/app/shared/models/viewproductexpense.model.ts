import { ExpenseModel } from './expense.model';

export class ViewProductExpenseModel {
    constructor(public Expenses: ExpenseModel[], public ProductId: number, public EquipmentType: string, public Model: string,
        public Year: number, public ProductName: string, public VinNumber: string,
        public PurchaseLocation: string, public PurchaseDate: Date, public Image: string) {
    }
}
