import { PaymentModel } from "./payment.model";

export class SaleModel {
    constructor(public Id: number, public ProductId: number, public Amount: number, public SaleLocation: string,
        public Description: string, public SaleDate: string, public Currency: string, public Payments : PaymentModel[]) { }
}
