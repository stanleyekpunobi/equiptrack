import { ExpenseModel } from "./expense.model";
import { SaleModel } from "./sale.model";

export class ProductModel {
    constructor(public Id: number, public EquipmentType: string, public Model: string,
        public Year: number, public ProductName: string, public VinNumber: string,
        public PurchaseLocation: string, public PurchaseDate: string, public Image: string,
        public TotalPayments: number = 0, public TotalExpenses : number, public TotalSale: number, public IsProfit:boolean,
         public Expenses : ExpenseModel [], public Sale : SaleModel
    ) { }
}
