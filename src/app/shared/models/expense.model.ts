export class ExpenseModel {
    constructor(public ProductId: number, public Amount: number, public Currency: string,
        public Description: string, public ExpenseDate: string,
        public ExpenseCategoryId: number, public Category: string, public Id: number) {
    }
}
