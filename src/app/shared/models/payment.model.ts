export class PaymentModel {
    constructor(public Id: number, public SaleId: number, public Date_Paid: string, public AmountPaid: number,
        public Description: string, public Date_Created: Date) {
    }
}
