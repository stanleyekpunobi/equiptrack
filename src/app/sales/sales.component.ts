import { Component, OnInit } from '@angular/core';
import { AddpaymentsComponent } from './addpayments/addpayments.component';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SaleService } from '../shared/services/sale.service';
import { PaymentModel } from '../shared/models/payment.model';
import { SaleModel } from '../shared/models/sale.model';
import { ActivatedRoute } from '@angular/router';
import { ProductModel } from '../shared/models/product.model';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { OperationModel } from '../shared/models/operationmodel.model';
import { NgForm, ControlContainer } from '@angular/forms';
import { formatDate } from '@angular/common';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { AddsalesComponent } from './addsales/addsales.component';
@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.css'],
  providers: [SaleService]
})

export class SalesComponent implements OnInit {

  saleModel: SaleModel = new SaleModel(null, null, null, null, null, null, null, null);

  paymentList: PaymentModel[] = [];

  dataSource : MatTableDataSource<PaymentModel>;

  panelOpenState = false;

  displayedColumns: string[] = ['date', 'amount', 'description','action'];

  isSale:boolean = false;

  productModel: ProductModel = new ProductModel(null, null, null, null, null, null, null, null, null, null,
    null, null, null, null, null);

  totalPaid = {
    Amount: 0
  };

  saleDate = {
    year: 0,
    month: 10,
    day: 19
  };

  currencyUsed = ['NGN', 'USD'];

  constructor(private modalService: NgbModal,
     private saleService: SaleService,
      private activeRoute: ActivatedRoute,
    private dialogService: NgxCoolDialogsService,
     public activeModal: NgbActiveModal,
     private matDialog: MatDialog) {
      this.dataSource = new MatTableDataSource<PaymentModel>();

     }

  ngOnInit() {
    //console.log(this.saleModel)
    this.onGetSales();

    // if(this.saleModel.Id == null || this.saleModel.Id == undefined){
    //   this.isSale = true;
    // }else{

    // }

  }

  openSaleModal(sale: SaleModel) {
    //this.modalService.open(content);
    const dialogRef = this.matDialog.open(AddsalesComponent, {
      width:'400px',
      data: {sale:this.saleModel, isedit: true}
    });
  }

  addPayment(saleModel: SaleModel) {
    const dialogRef = this.matDialog.open(AddpaymentsComponent, {
      width:'400px',
      data: {sale:this.saleModel, payments: this.paymentList, totalpaid:this.totalPaid, isedit: false}
    });
    // const paymentModalRef = this.modalService.open(AddpaymentsComponent);
    // paymentModalRef.componentInstance.saleModel = saleModel;
    // paymentModalRef.componentInstance.paymentList = this.paymentList;
    // paymentModalRef.componentInstance.totalPaid = this.totalPaid;

  }

  editPayment(paymentModel: PaymentModel, isEdit: boolean, saleModel: SaleModel) {
    // const paymentModalRef = this.modalService.open(AddpaymentsComponent);
    // paymentModalRef.componentInstance.paymentModel = paymentModel;
    // paymentModalRef.componentInstance.isEdit = isEdit;
    // paymentModalRef.componentInstance.saleModel = saleModel;
    // paymentModalRef.componentInstance.paymentList = this.paymentList;
    const dialogRef = this.matDialog.open(AddpaymentsComponent, {
      width:'400px',
      data: {sale:saleModel, payments: this.paymentList, totalpaid:this.totalPaid, isedit: true}
    });

  }

  onDeletePayment(paymentid: number) {
    const operationModel = new OperationModel(paymentid);

    this.dialogService.confirm('This will delete the selected payment for this sale')
      .subscribe(res => {
        if (res) {
          this.saleService.DeletePayment(operationModel).subscribe(
            (response) => {
              this.paymentList.length = 0;
              this.onGetSales();
              this.dialogService.alert('Payment deleted', {
                theme: 'default',
                title: 'EquipTrack'
              });
            },
            (error) => this.dialogService.alert('Unable to update expense')
          );
        } else {
          this.dialogService.alert('Delete operation canceled');
        }
      });
  }

  onGetSales() {
    this.totalPaid.Amount = 0;
    this.paymentList.length = 0;
    const productId = this.activeRoute.snapshot.params['id'];
    this.saleService.GetSale(productId).subscribe(
      (data) => {
        console.log(data);
        if (data.sale != null) {

          this.dataSource.data = data.sale.Payments;

          this.saleModel.Amount = data.sale.Amount;
          this.saleModel.Description = data.sale.Description;
          this.saleModel.Id = data.sale.Id;
          this.saleModel.ProductId = data.sale.productId;
          this.saleModel.SaleDate = data.sale.SaleDate;
          this.saleModel.SaleLocation = data.sale.SaleLocation;
          this.saleModel.Currency = data.sale.Currency;
          this.productModel = data.sale.Product;
          data.sale.Payments.forEach(paymentItem => {
            this.paymentList.push(paymentItem);
            this.totalPaid.Amount += paymentItem.AmountPaid;
          });
          const dateString = formatDate(this.saleModel.SaleDate.toString(), 'yyyy-MM-dd', 'en-US');
          const splitDate = dateString.split('-');
          this.saleDate.year = parseInt(splitDate[0], 10);
          this.saleDate.month = parseInt(splitDate[1], 10);
          this.saleDate.day = parseInt(splitDate[2], 10);
          // console.log(this.saleModel.Currency);
        }
      },
      (error) => {
        this.dialogService.alert('Unable to get sale data', {
          title: 'EquipTrack',
          theme: 'default'
        });
      }

    );
  }

  onDeleteSale(saleid: number) {
    const operationModel = new OperationModel(saleid);

    this.dialogService.confirm('This will delete the selected sale for this product')
      .subscribe(res => {
        if (res) {
          this.saleService.DeleteSale(operationModel).subscribe(
            (response) => {
              this.paymentList.length = 0;
              this.onGetSales();
              this.dialogService.alert('Sale deleted', {
                theme: 'default',
                title: 'EquipTrack'
              });
            },
            (error) => this.dialogService.alert('Unable to delete Sale')
          );
        } else {
          this.dialogService.alert('Delete operation canceled');
        }
      });
  }
}
