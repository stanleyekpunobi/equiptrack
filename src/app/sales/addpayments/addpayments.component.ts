import { Component, OnInit, AfterViewInit, Input, OnDestroy, DoCheck, ViewChild, ElementRef, Inject } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SaleService } from 'src/app/shared/services/sale.service';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { PaymentModel } from 'src/app/shared/models/payment.model';
import { SaleModel } from 'src/app/shared/models/sale.model';
import { ProductModel } from 'src/app/shared/models/product.model';
import { NgForm, ControlContainer } from '@angular/forms';
import { formatDate } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-addpayments',
  templateUrl: './addpayments.component.html',
  styleUrls: ['./addpayments.component.css'],
  providers: [SaleService]
})

export class AddpaymentsComponent implements OnInit, OnDestroy {

  constructor(@Inject(MAT_DIALOG_DATA) public data:any, public activeModal: NgbActiveModal, public activeRoute: ActivatedRoute,
    private dialog: NgxCoolDialogsService, private saleService: SaleService, private router: Router) { }

  paymentList: PaymentModel[];

  saleModel: SaleModel;

  paymentModel: PaymentModel;

  isEdit: boolean;

  @ViewChild('pDate') datePicker : ElementRef;

  totalPaid = {
    Amount: 0
  };

  paymentDate = {
    year: 0,
    month: 10,
    day: 19
  };

  ngOnInit() {
    console.log(this.data);
  }

  // ngAfterViewInit() {

  //   if (this.isEdit) {
  //     const dateString = formatDate(this.paymentModel.Date_Paid.toString(), 'yyyy-MM-dd', 'en-US');
  //     const splitDate = dateString.split('-');
  //     this.paymentDate.year = parseInt(splitDate[0], 10);
  //     this.paymentDate.month = parseInt(splitDate[1], 10);
  //     this.paymentDate.day = parseInt(splitDate[2], 10);
  //   }
  //   // console.log(this.paymentList);

  // }

  ngOnDestroy() {
    console.log('component destroyed');
    this.onGetPayment();

  }

  onAddPayment(form: NgForm) {

    console.log(form);

    const paymentDate = formatDate(this.datePicker.nativeElement.value, 'yyyy-MM-dd', 'en-US');

    //const purchaseDate = new Date(form.value.Date_Paid.year, form.value.Date_Paid.month - 1, form.value.Date_Paid.day);

    const paymentItem = new PaymentModel(null, this.data.sale.Id, paymentDate, form.value.Amount, form.value.Description, null);

    this.saleService.AddPayment(paymentItem).subscribe(
      (res) => {
        if (res) {
          this.activeModal.close();
          this.dialog.alert('Payment successfuly added', {
            title: 'EquipTrack',
            theme: 'default'
          });
        }
      },
      (error) => {
        this.activeModal.close();
        this.dialog.alert('Unable to add payment', {
          title: 'EquipTrack',
          theme: 'default'
        });
      }
    );

  }

  onEditPayment(form: NgForm) {

    const paymentDate = formatDate(this.datePicker.nativeElement.value, 'yyyy-MM-dd', 'en-US');

   // const paymentDate = new Date(form.value.Date_Paid.year, form.value.Date_Paid.month - 1, form.value.Date_Paid.day);

    const paymentItem = new PaymentModel(form.value.Id, this.saleModel.Id, paymentDate, form.value.Amount, form.value.Description, null);

    // console.log(form.value.Id);

    this.saleService.UpdatePayment(paymentItem).subscribe(
      (response) => this.dialog.alert('Payment successfuly changed', {
        theme: 'default',
        title: 'EquipTrack'
      }).subscribe(
        (res) => {
          this.paymentModel.AmountPaid = paymentItem.AmountPaid;
          this.paymentModel.Date_Paid = paymentItem.Date_Paid;
          this.paymentModel.Description = paymentItem.Description;
          if (res) {
            this.activeModal.dismiss('Update complete');
          }
        }),
      (error) => this.dialog.alert('Unable to update payment')
    );

  }

  onGetPayment() {
    // const productId = this.activeRoute.snapshot.params['id'];
    // console.log(productId);
    this.saleService.GetSale(this.saleModel.Id).subscribe(
      (data) => {
        // console.log(data);
        if (data.sale != null) {
          this.paymentList.length = 0;

          this.saleModel.Amount = data.sale.Amount;
          this.saleModel.Description = data.sale.Description;
          this.saleModel.Id = data.sale.Id;
          this.saleModel.ProductId = data.sale.productId;
          this.saleModel.SaleDate = data.sale.SaleDate;
          this.saleModel.SaleLocation = data.sale.SaleLocation;
          this.saleModel.Currency = data.sale.Currency;
          data.sale.Payments.forEach(paymentItem => {
            this.paymentList.push(paymentItem);
            this.totalPaid.Amount += paymentItem.AmountPaid;
            // console.log(this.paymentList);
          });
        }
      },
      (error) => {
        this.dialog.alert('Unable to get sale data', {
          title: 'EquipTrack',
          theme: 'default'
        });
      }

    );
  }

}
