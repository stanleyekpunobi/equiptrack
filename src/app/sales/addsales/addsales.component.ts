import { Component, OnInit, Inject, ElementRef, ViewChild } from '@angular/core';
import { NgForm, FormGroup, FormControl } from '@angular/forms';
import { SaleService } from 'src/app/shared/services/sale.service';
import { SaleModel } from 'src/app/shared/models/sale.model';
import { ProductService } from 'src/app/shared/services/product.service';
import { ProductModel } from 'src/app/shared/models/product.model';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { formatDate } from '@angular/common';
@Component({
  selector: 'app-addsales',
  templateUrl: './addsales.component.html',
  styleUrls: ['./addsales.component.css'],
  providers: [SaleService]
})
export class AddsalesComponent implements OnInit {

  products: ProductModel[] = [];

  imageSrc = '';

  editForm: FormGroup;

  @ViewChild('d') datePicker : ElementRef;


  constructor(@Inject(MAT_DIALOG_DATA) public data:any,private saleService: SaleService, private productService: ProductService, private dialog: NgxCoolDialogsService) { }

  ngOnInit() {
    console.log(this.data);
   // this.onGetAllProducts();
    if(this.data != null) {
      if(this.data.isedit == true){

        console.log(this.data);

        this.editForm = new FormGroup({
          'Amount': new FormControl(this.data.sale.Amount),
          'Currency': new FormControl(this.data.sale.Currency),
          'Description': new FormControl(this.data.sale.Description),
          'SaleDate': new FormControl(this.data.sale.SaleDate),
          'Id': new FormControl(this.data.sale.Id),
          'SaleLocation': new FormControl(this.data.sale.SaleLocation),
        })

      }


    }
  }

  onProductSelected(event: Event) {
    console.log((<HTMLSelectElement>event.target).value.split('>', 2)[1]);
    this.imageSrc = (<HTMLSelectElement>event.target).value.split('>', 2)[1];
  }

  onGetAllProducts() {
    this.productService.GetAllProducts().subscribe(
      (data: ProductModel[]) => {
        for (const p of data) {
          this.products.push(p);
        }
        // console.log(data);
      },
      (error) => console.log(error)
    );
  }
  OnAddSale(form: NgForm) {
    console.log(form);

   // const saleDate = new Date(form.value.SaleDate.year, form.value.SaleDate.month - 1, form.value.SaleDate.day);
   const saleDate = formatDate(this.datePicker.nativeElement.value, 'yyyy-MM-dd', 'en-US');

    const saleModel = new SaleModel(this.data.Id, this.data.Id, form.value.Amount, form.value.SaleLocation,
      form.value.Description, saleDate, form.value.Currency, null);

    this.saleService.AddSale(saleModel).subscribe(
      (response) => this.dialog.alert('Sale added to product!', {
        theme: 'default',
        title: 'Auction dash'
      }),
      (error) => this.dialog.alert('Unable to add sale to product')
    );

  }

  onUpdateSale(form: NgForm) {

    const saleDate = formatDate(this.datePicker.nativeElement.value, 'yyyy-MM-dd', 'en-US');

    //const saleDate = new Date(form.value.SaleDate.year, form.value.SaleDate.month - 1, form.value.SaleDate.day);

    const newSaleModel = new SaleModel(form.value.Id, form.value.ProductId, form.value.Amount, form.value.SaleLocation,
      form.value.Description, saleDate, form.value.Currency, null);

    this.saleService.UpdateSale(newSaleModel).subscribe(
      (response) => this.dialog.alert('Sale successfuly updated!', {
        theme: 'default',
        title: 'Auction dash'
      }).subscribe(res => {
        if (res) {
          this.data.sale = newSaleModel;
        }
      }),
      (error) => this.dialog.alert('Unable to update sale')
    );
  }

}



